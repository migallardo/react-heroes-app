import React, {useContext} from 'react'
import {AuthContext} from "../../auth/AuthContext";
import {types} from "../../types/types";

export const LoginScreen = ({history}) => {

    const {dispatch} = useContext(AuthContext);

    const handleLogin = (e) => {
        dispatch({
            type: types.login,
            payload: {
                name: 'Marcelo'
            }
        });
        history.replace(localStorage.getItem('lastPath') || '/');
    }


    return (
        <div className="container text-center">
            <h1>Login</h1>
            <button
                className="btn btn-primary"
                onClick={handleLogin}
            >
                Ingresar
            </button>
        </div>
    )
}
