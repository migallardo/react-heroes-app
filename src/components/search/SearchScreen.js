import React, {useMemo} from 'react';
import queryString from 'query-string'
import {HeroCard} from "../heroes/HeroCard";
import {useForm} from "../../hooks/useForm";
import {useLocation} from "react-router-dom";
import {getHeroesByCriteria} from "../../selectors/getHeroesByCriteria";

export const SearchScreen = ({history}) => {

    const location = useLocation();
    const {q = ''} = queryString.parse(location.search)

    const [formValues, handleInputChange] = useForm({
        searchInput: q
    });
    const {searchInput} = formValues;

    const heroes = useMemo(() => getHeroesByCriteria(q), [q]);

    const handleSearch = (e) => {
        e.preventDefault();
        const {searchInput} = formValues;
        history.push(`?q=${encodeURI(searchInput)}`)
    }

    return (
        <div>
            <h1>Search</h1>
            <div className="row">
                <div className="col-5">
                    <h4>Search Form</h4>
                    <hr/>
                    <form onSubmit={handleSearch}>
                        <input type="text" placeholder="Type a hero" className="form-control" name="searchInput"
                               value={searchInput}
                               onChange={handleInputChange} autoComplete="off"/>
                        <button type="submit" className="btn m-1 btn-block btn-outline-primary">Search!</button>
                    </form>
                </div>
                <div className="col-7">
                    <h4>Results</h4>
                    <hr/>
                    {
                        (q === '') &&  <div className="alert alert-info">Search a Hero.</div>
                    }

                    {
                        (q !== '') && heroes.length === 0 && <div className="alert alert-warning">No results found.</div>
                    }

                    {
                        heroes.map(hero => (
                            <HeroCard
                                key={hero.id}
                                {...hero}
                            />
                        ))
                    }
                </div>
            </div>
        </div>
    );
};
