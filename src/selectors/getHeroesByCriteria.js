import {heroes} from "../data/heroes";


export const getHeroesByCriteria = (criteria = '') => {
    criteria = criteria.toLocaleLowerCase();
    if (criteria === '') {
        return [];
    }

    return heroes.filter(hero => hero.superhero.toLocaleLowerCase().includes(criteria));
}