import {authReducer} from "../../auth/authReducer";
import {types} from "../../types/types";

describe('authReducer Tests', () => {

    const username = 'Test User';

    test('should return default state', () => {
        const state = authReducer({logged: false}, {});
        expect(state).toEqual({logged: false});
    });

    test('should authenticate and set user name', () => {
        const action = {
            type: types.login,
            payload: {
                name: username
            }

        }
        const state = authReducer({logged: false}, action);
        expect(state).toEqual({logged: true, name: username});
    });

    test('should delete user name and set logged to false', () => {
        const action = {
            type: types.logout
        }
        const state = authReducer({logged: true, name: username}, action);
        expect(state).toEqual({logged: false});
    });


})